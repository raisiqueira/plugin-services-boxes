<?php
/**
   * Service Widgets uninstalled.
   *
   * @package   service_boxes
   * @author    Raí Siqueira <contato@raisiqueira.com>
   * @license   MIT License
   * @link      http://raisiqueira.com/
   * @copyright 2017 Rai Siqueira
 */

// If uninstall
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// Delete plugin settings
delete_option( 'service_boxes_widget' );
delete_site_option( 'service_boxs_widget' );